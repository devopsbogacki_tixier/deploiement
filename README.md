# Déploiement d'Application avec GitLab CI/CD et Terraform

Ce projet décrit comment déployer une application conteneurisée sur Azure en utilisant GitLab CI/CD et Terraform. Vous trouverez ici des instructions pour exécuter le pipeline CI/CD en local à l'aide de `gitlab-ci-local`.

## Prérequis

Assurez-vous d'avoir les éléments suivants installés sur votre machine :

- [Docker](https://docs.docker.com/get-docker/)
- [Terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli)
- [Azure CLI](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli)
- [gitlab-ci-local](https://github.com/firecow/gitlab-ci-local)

Il est aussi nécessaire de remplacer les données sensible avec les votre.

## Installation

- Cloner les 3 repos dans un même dossier
    `Backend`
    `Frontend`
    `Deploiement`

## Déploiement

Déplacez-vous dans le dossier `/deploiement`:
```sh
cd deploiement
```
Assurez-vous de de vous être connecté à azure avec la commande suivant : 
```sh
az login
```
Exécutez ensuite le déploiement : 
```sh
gitlab-ci-local
```