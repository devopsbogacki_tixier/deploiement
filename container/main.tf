provider "azurerm" {
  features {}
}

data "azurerm_container_registry" "main" {
  name                = "devopsbogackitixier"
  resource_group_name = "devopsbogackitixierrg"
}


resource "azurerm_container_group" "container_group" {
  name                = "calculator"
  location            = "East US"
  resource_group_name = "devopsbogackitixierrg"
  ip_address_type     = "Public"
  dns_name_label      = "calculator"
  os_type             = "Linux"

  container {
    name   = "backend"
    image  = "devopsbogackitixier.azurecr.io/backend:latest"
    cpu    = "0.5"
    memory = "1.5"

    ports {
      port     = 10003
      protocol = "TCP"
    }
  }

  container {
    name   = "frontend"
    image  = "devopsbogackitixier.azurecr.io/frontend:latest"
    cpu    = "0.5"
    memory = "1.5"

    ports {
      port     = 3000
      protocol = "TCP"
    }
  }

  container {
    name   = "mysql-db"
    image  = "mysql:latest"
    cpu    = "0.5"
    memory = "1.5"

    ports {
      port     = 3306
      protocol = "TCP"
    }

    environment_variables = {
      MYSQL_ROOT_PASSWORD = "root"
      MYSQL_DATABASE      = "root"
      MYSQL_DATABASE      = "calculator"
      DB_HOST     = "localhost"
    }
  }
}
