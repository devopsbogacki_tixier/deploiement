provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "main" {
  name     = "devopsbogackitixierrg"
  location = "East US"
}

resource "azurerm_container_registry" "main" {
  name                = "devopsbogackitixier"
  resource_group_name = azurerm_resource_group.main.name
  location            = azurerm_resource_group.main.location
  sku                 = "Basic"
  admin_enabled       = true
}